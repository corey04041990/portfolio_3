$('.slider-for').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    asNavFor: '.slider-nav'
});
$('.slider-nav').slick({
    slidesToShow: 5,
    slidesToScroll: 5,
    asNavFor: '.slider-for',
    centerMode: true,
    centerPadding: '0',
    focusOnSelect: true,
    prevArrow: '<i class="prev-arrow fas fa-chevron-left"></i>',
    nextArrow: '<i class="next-arrow fas fa-chevron-right"></i>'
});